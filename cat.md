# Cat

- An [[animal]].
  - [[ocell]].
- A [[person]].
  - [[mastodon]] https://pinafore.social/accounts/187732


[//begin]: # "Autogenerated link references for markdown compatibility"
[animal]: animal "Animal"
[ocell]: ocell "Ocell"
[person]: person "Person"
[mastodon]: mastodon "Mastodon"
[//end]: # "Autogenerated link references"