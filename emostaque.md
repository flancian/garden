# Emostaque

- A [[person]].
- [[twitter]] twitter.com/emostaque
- Thinks of [[poverty]] as an [[information problem]]: https://twitter.com/EMostaque/status/1352606411545571329
- Likes [[clayton christensen]]


[//begin]: # "Autogenerated link references for markdown compatibility"
[person]: person "Person"
[twitter]: twitter "Twitter"
[poverty]: poverty "Poverty"
[//end]: # "Autogenerated link references"