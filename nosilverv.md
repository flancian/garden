# Nosilverv

- A [[person]].
- [[twitter]] twitter.com/nosilverv
- [[go]] rivalvoices.substack.com
- [[partner]] [[exgenesis]]
- [[pull]] [[rival voices]]
- [[likes]] [[jordan peterson]], which I found surprising

[//begin]: # "Autogenerated link references for markdown compatibility"
[person]: person "Person"
[twitter]: twitter "Twitter"
[go]: go "Go"
[exgenesis]: exgenesis "Exgenesis"
[pull]: pull "Pull"
[rival voices]: rival-voices "Rival Voices"
[jordan peterson]: jordan-peterson "Jordan Peterson"
[//end]: # "Autogenerated link references"