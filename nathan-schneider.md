# Nathan Schneider

- [[person]]
- [[mastodon]] social.coop/ntnsndr
- [[zotero]] https://www.zotero.org/ntnsndr/

[//begin]: # "Autogenerated link references for markdown compatibility"
[person]: person "Person"
[mastodon]: mastodon "Mastodon"
[//end]: # "Autogenerated link references"