# Ipfs

- a [[project]]
  - [[by]] [[juan benet]]
    - [[protocol labs]]
- [[do]] look into it, think of possible applications. It seems like a natural fit for the [[agora]].
  - Think of the [[mvp]] here.
- [[armengolaltayo]] was using it for his project.
- [[elopio]] is a developer.
- [[go]] https://en.wikipedia.org/wiki/InterPlanetary_File_System
- [[gateways]]  https://dweb-primer.ipfs.io/classical-web/public-gateways
  - If you're using the hash of a specific snapshot of content, use the path https://ipfs.io/ipfs/<your-ipfs-hash>. 
  - If you're using an IPNS hash to get the latest version of some content, use the path https://ipfs.io/ipns/<your-ipns-hash>


[//begin]: # "Autogenerated link references for markdown compatibility"
[project]: project "Project"
[do]: do "Do"
[agora]: agora "Agora"
[mvp]: mvp "MVP"
[armengolaltayo]: armengolaltayo "Armengolaltayo"
[elopio]: elopio "Elopio"
[go]: go "Go"
[//end]: # "Autogenerated link references"