# Model of the World

- [[pull]] We all carry [[models of the world]] with us at all times. In fact, our models of the world might be the only thing we ever get to interact with; it is with our [[models]] that we [[make sense]] of things and take (or observe) [[decisions]].


[//begin]: # "Autogenerated link references for markdown compatibility"
[pull]: pull "Pull"
[//end]: # "Autogenerated link references"