# Lambertian

- [[go]] https://en.wikipedia.org/wiki/Lambertian_reflectance
  - [[quote]] Lambertian reflectance is the property that defines an [[ideal]] "[[matte]]" or diffusely reflecting surface. 


[//begin]: # "Autogenerated link references for markdown compatibility"
[go]: go "Go"
[quote]: quote "Quote"
[//end]: # "Autogenerated link references"