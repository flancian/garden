# Recipes

- Procedures to transform [[ingredients]] and [[time]] into [[food]].
- [[pull]] [[agora recipes]]


[//begin]: # "Autogenerated link references for markdown compatibility"
[time]: time "Time"
[pull]: pull "Pull"
[agora recipes]: agora-recipes "Agora Recipes"
[//end]: # "Autogenerated link references"