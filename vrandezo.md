# Vrandezo

- A [[person]].
- [[ontologist]]
- [[pull]] [[denny vrandečić]]
- [[go]] https://de.wikipedia.org/wiki/Denny_Vrande%C4%8Di%C4%87
- [[twitter]] twitter.com/vrandezo
- [[quote]] [[optional conventions]] are [[templates]]
  - https://twitter.com/vrandezo/status/1352704684012654593
- [[project]] [[abstract wikipedia]]
- [[founder]] [[wikidata]]



[//begin]: # "Autogenerated link references for markdown compatibility"
[person]: person "Person"
[pull]: pull "Pull"
[denny vrandečić]: denny-vrandečić "Denny Vrandečić"
[go]: go "Go"
[twitter]: twitter "Twitter"
[quote]: quote "Quote"
[project]: project "Project"
[//end]: # "Autogenerated link references"