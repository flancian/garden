# Agorism

- [[go]] https://en.wikipedia.org/wiki/Agorism
  - [[agora]], [[anarchy]], [[action]]!
  - Founded by [[samuel edward konkin iii]].
- A pseudo-inspiration for the [[agora]] in the sense that it's very compatible in many ways, but I started working on the [[agora]] (and chose the name) before I was aware of agorism as a [[movement]] or [[ideology]].


[//begin]: # "Autogenerated link references for markdown compatibility"
[go]: go "Go"
[agora]: agora "Agora"
[action]: action "Action"
[//end]: # "Autogenerated link references"