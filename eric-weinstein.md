# Eric Weinstein

- A [[person]].
- [[physicist]]
- Friend of [[sam harris]].
- Works for [[peter thiel]].
- [[pull]] [[eric r. weinstein]]
- [[a neither]] recommended https://www.edge.org/response-detail/11783


[//begin]: # "Autogenerated link references for markdown compatibility"
[person]: person "Person"
[sam harris]: sam-harris "Sam Harris"
[peter thiel]: peter-thiel "Peter Thiel"
[pull]: pull "Pull"
[eric r. weinstein]: eric-r-weinstein "Eric R. Weinstein"
[a neither]: a-neither "A Neither"
[//end]: # "Autogenerated link references"