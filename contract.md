# CONTRACT

- I endorse the base [[contract]] of this Agora.
- I [[intend]] to dedicate a [[significant fraction]] of my [[free time]] and [[money]] to improve the world.
- I am a [[Flancian]] and I have a [[manifesto]].
  - In [[Flancia]] there is no [[poverty]].
  - In [[Flancia]] there is no [[privilege]].
  - In [[Flancia]] there is an [[Agora]].
  - In [[Flancia]] we shall one day, if you want, [[meet]].
- Because I am unskillful, I need your help. I am open to any [[feedback]] anytime.
  - https://twitter.com/flancian/status/1357031412914864131

[//begin]: # "Autogenerated link references for markdown compatibility"
[contract]: contract "CONTRACT"
[free time]: free-time "Free Time"
[Flancian]: flancian "Flancian"
[manifesto]: manifesto "Manifesto"
[Flancia]: flancia "Flancia"
[poverty]: poverty "Poverty"
[privilege]: privilege "Privilege"
[Agora]: agora "Agora"
[meet]: meet "Meet"
[feedback]: feedback "Feedback"
[//end]: # "Autogenerated link references"