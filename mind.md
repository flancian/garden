# Mind

- [[pull]] [[minds]]
- What [[you]] and [[me]] are.
  - See also: [[identity]].


[//begin]: # "Autogenerated link references for markdown compatibility"
[pull]: pull "Pull"
[you]: you "You"
[me]: me "Me"
[identity]: identity "Identity"
[//end]: # "Autogenerated link references"