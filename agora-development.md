# Agora Development

- A [[group]].
- A [[stoa]].
  - Heavily interacts with [[fedstoa]].
  - [[project]] [[agora]]
    - [[project]] [[agora bridge]]
    - [[project]] [[agora protocol]]
      - [[project]] [[wikilinks everywhere]]
- A [[matrix]] channel.
  - [[go]] https://app.element.io/?pk_vid=2aa9ef53afdcaeaf1609608427b036f4#/room/!LrSlpgPruzcRvucsiG:matrix.org


[//begin]: # "Autogenerated link references for markdown compatibility"
[project]: project "Project"
[agora]: agora "Agora"
[agora bridge]: agora-bridge "Agora Bridge"
[agora protocol]: agora-protocol "Agora Protocol"
[wikilinks everywhere]: wikilinks-everywhere "Wikilinks Everywhere"
[matrix]: matrix "Matrix"
[go]: go "Go"
[//end]: # "Autogenerated link references"