# Gerhard Zorn

- A [[person]].
  - [[friend]]
- [[twitter]] twitter.com/zorngerhard
- Met them through [[codex]]
- Introduced me to [[concepts-techniques-and-models-of-computer-programming]], [[mozart]].


[//begin]: # "Autogenerated link references for markdown compatibility"
[person]: person "Person"
[friend]: friend "Friend"
[twitter]: twitter "Twitter"
[codex]: codex "Codex"
[concepts-techniques-and-models-of-computer-programming]: concepts-techniques-and-models-of-computer-programming "Concepts, Techniques and Models of Computer Programming"
[mozart]: mozart "Mozart"
[//end]: # "Autogenerated link references"