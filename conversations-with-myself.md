# Conversations with Myself

- A [[book]].
  - [[by]] [[nelson mandela]]
  - [[goodreads]] https://www.goodreads.com/book/show/8113026-conversations-with-myself
- An [[album]]
  - [[by]] [[bill evans]]
  - [[go]] https://music.youtube.com/playlist?list=OLAK5uy_ntIuYUsjgRHsARRY0FZWK-uOnK7FAmokA
- I usually start conversations with myself in chat platforms as a way to have ubiquituous [[note taking]].


[//begin]: # "Autogenerated link references for markdown compatibility"
[book]: book "Book"
[bill evans]: bill-evans "Bill Evans"
[go]: go "Go"
[//end]: # "Autogenerated link references"