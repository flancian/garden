# Polycultic

- [[person]].
- [[twitter]] twitter.com/polycultic
  - [[push]] [[sun tzu]]
    - [[quote]] in the midst of [[chaos]], there is also [[opportunity]].
- [[go]] wememestonks.com
- [[friendly]]


[//begin]: # "Autogenerated link references for markdown compatibility"
[person]: person "Person"
[twitter]: twitter "Twitter"
[push]: push "Push"
[quote]: quote "Quote"
[chaos]: chaos "Chaos"
[go]: go "Go"
[//end]: # "Autogenerated link references"