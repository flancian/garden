# The Software Shall Be Used for Good, Not Evil

- A [[statement]] included in the [[license]] [[douglas crockford]] uses to release his software.
- [[go]] https://m.youtube.com/watch?v=-C-JoyNuQJs&t=39m50s
- [[controversial]]
  - [[opensource.org]] says (https://opensource.org/faq#evil) it's not open source: "The Open Source Definition specifies that Open Source licenses may not discriminate against persons or groups. Giving everyone freedom means giving evil people freedom, too."
- [[pull]] [[douglas crockford]]


[//begin]: # "Autogenerated link references for markdown compatibility"
[douglas crockford]: douglas-crockford "Douglas Crockford"
[go]: go "Go"
[pull]: pull "Pull"
[//end]: # "Autogenerated link references"