# Foam

- A [[project]].
- [[roam like]]. A [[vscode]] extension.
- [[go]] https://foambubble.github.io/foam/
  - By [[jevakallio]].
  - Notable contributors / people I've met in the [[discord]]: [[ingalless]] [[scottjoe]] [[riccardo]] [[mathieudutour]].
- As of [[2021-01-04]], my personal [[agora client]].
  - See [[foam settings]], which describes my setup.

[//begin]: # "Autogenerated link references for markdown compatibility"
[project]: project "Project"
[roam like]: roam-like "Roam Like"
[vscode]: vscode "Vscode"
[go]: go "Go"
[jevakallio]: jevakallio "Jevakallio"
[discord]: discord "Discord"
[scottjoe]: scottjoe "Scottjoe"
[mathieudutour]: mathieudutour "Mathieudutour"
[2021-01-04]: journal/2021-01-04 "2021-01-04"
[agora client]: agora-client "Agora Client"
[foam settings]: foam-settings "Foam Settings"
[//end]: # "Autogenerated link references"