# Hypnagogic

- [[go]] jmoore.me
- [[liberapay]] https://liberapay.com/hypnagogic
- [[project]] [[moa.party]]
- [[mastodon]] https://pdx.social/@foozmeat
- [[fediverse]] https://pdx.social/@foozmeat
- [[github]] https://github.com/foozmeat


[//begin]: # "Autogenerated link references for markdown compatibility"
[go]: go "Go"
[liberapay]: liberapay "Liberapay"
[project]: project "Project"
[mastodon]: mastodon "Mastodon"
[fediverse]: fediverse "Fediverse"
[//end]: # "Autogenerated link references"