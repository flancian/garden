# Jonathan the Utopian
- A [[person]].
- [[swiss]] [[mathematician]].
  - [[category theory]]
- [[go]] lorand.earth
- [[twitter]] https://twitter.com/l_o_r_a_n_d
- [[gluten-free]], not into [[dairy]].
- Found and pointed out [[Smart Agora]]: https://coss.ethz.ch/research/pastprojects/SmartAgora.html
- [[project]] [[utopoi]]: http://lorand.earth/utopoi/about/
- [[project]] https://applied-compositional-thinking.engineering/

## [[meet]]


### [[next]]

- how to organize [[todo]].

### [[2021-02-06]]

- [[beautiful light]] today, perhaps because of the [[sand storm]]
- [[day job]] is looking better -- [[course]] sessions over!
  - [[goal]] was to show what [[category theory]] could do for [[engineering]]
    - it was a [[pilot]]
    - made a [[feedback form]]
  - [[two weeks off]]!
    - [[protopoi space]]
    - [[christopher alexander reading group]]
    - [[writing assignment]]
  - [[internalized capitalism]] 
    - how many days really-off per week?
    - taking one full day off a week seems
- [[graphical calculus]] / [[string diagrams]] in [[category theory]]
- [[people]]
  - [[metasj]]
    - [[knowledge futures project]]
  - [[eugenia cheng]]
  - [[bmann]]
  - [[karlicoss]]
- [[protopoi]]
  - [[next steps]]
- [[stoa]]
  - [[stoa nodes]]
  - [[two ways to collaborate]]
    - do they [[converge]]?
- [[group activities]]
- [[protopoi org]]
  - could be [[graph first]], more [[visual]], hopefully [[beautiful]]
  - https://ninaemge.branching.ch
  - https://kasparkoenig.com/
  - https://branching.ch/


### [[2021-01-23]]

- Greetings!
  - Helping a friend move today.
- Working hard on setting up the curriculum for the course at [[ETH]]
  - [[chestertons fence]]
- [[jonathan]] want to start building a website: [[protopoi space]]
  - [[requirements]] 
  - [[domain]]
    - [[jonathan]] [[protopoi.space]]
    - [[flancian]] [[protopoi.org]] [[protopoi.com]] perhaps [[protopoi.coop]]
- [[ideas]]
  - [[jonathan]] [[platform]], [[place]] where one can link to different projects. [[root node]] for [[art school]] [[project]].
    - Building [[spaces]].
    - [[pattern languaguages]].
  - [[flancian]] a [[root node]] for all [[protopias]] everywhere :)
  - what is [[protopoi]]?
    - [[flancian]] the [[space]] of all [[protopias]]?
      - an [[index]]
    - [[jonathan]] a [[vision]] of how the [[world]] could be?
      - a [[space]] in the sense of a [[container]] and [[home]] for [[events]] and [[experiments]] which test certain kinds of [[processes]] that are about questioning the [[status quo]] and exploring how to change it incrementally and constructively
        - questioning the status quo can be an [[introspective]], [[meditative]] activity
  - [[jonathan]] the [[project]] is focused on a [[process]] for imagining and exploring [[protopoi]]
  - a [[repository]] of [[projects]] following a theme or vision.
- [[borges]] 
- [[introductions]]
  - [[anirudh badri]]
    - in [[india]]
    - affinity: [[book club]]
  - [[armengolaltayo]]
    - in [[spain]]
    - affinity: [[art]]
  - [[bmann]]
    - in [[canada]]
- [[push]] [[christopher alexander reading group]]
  - [[quote]] We will read from some works of the architect and design theorist Christopher Alexander, with the aim of getting to know the ideas and philosophy associated with the influential book "A pattern language". The term "pattern language" describes an approach to (architectural) design which identifies and abstracts elementary yet ubiquitous design problems, along with solutions for them, and assembles them into a sort of alphabet to be used as a toolbox. 
  - [[quote]] This methodology is embedded in a design philosophy which attempts to put humans' needs, desires, and "aliveness" at the center of the design process. Although coming from architecture and urban design, Christopher Alexander's work has aspects which are of a very general, conceptual nature. It has been an influence in other fields, such as software design and music, and perhaps this module can be an opportunity to explore possible inspirations within one's own disciplinary context.
  - [[quote]] Proposed readings: excerpts from the following books by Christopher Alexander: "Notes on the Synthesis of Form"; "The Timeless Way of Building"; "A Pattern Language". Possibly also additional materials.
  - [[the timeless way of building]]
  - [[a pattern language]]
  - [[notes on the synthesis of form]]
- [[feedback]] on [[priorities]] with [[agora feature request]] and [[agora plan]]
  - [[chat]] per [[node]] plus [[multiple fanout]]
  - [[flancian]] I should get a blackboard

### [[2021-01-01]]
- Super cool that you have a blackboard in your room! 
- VC is very inclusive and we can always have this, no matter where we live.
- What have you been working on?
  - [[jonathan]] working a lot on the online course at [[eth]]. Sort of managing the project almost.
    - [[flancian]] do you feel like you are learning at the day job?
    - yep. Experimenting with new mediums for [[online teaching]].
    - [[category theory for engineers]]
      - [[flancian]] related to [[category theory for programmers]]?
      - Different target audience: these are engineers in the sense of mechanical engineering, aerospace. Less direct mappings here.
      - Main applicational framework: [[complex systems]] + [[modeling]], [[design]], [[optimization]].
- [[world models]]
- [[utopoi]]
- [[projections]]
  - [[flancian]] does the word make sense?
  - [[jonathan]] yes. but [[filtering]] may be less confusing.
- [[agora]]
  - [[users]]
  - [[groups]]
    - node [[bourbaki]]
  - [[stoas]]
- [[pivoting]]
  - [[push]] [[braiding]]
    - something is [[symmetric]] if [[braiding twice is identity]]
    - [[braid]] [[hofstadter]] 
  - [[twist]]
- [[agora]] for [[utopia]]
  - [[missing devices]] 
  - [[utopoi]]
    - [[flancian]] set of [[m prime]]
      - [[jonathan]] mathematical + computational approach to the world
        - on the one hand it seems it will always miss something, as life is so rich
        - on the other hand it seems it could be valuable
        - at the beginning it seems it could be unpractical, but looking around similar approaches are around
        - it is very costly to input lots of information, and it requires a lot of time
      - [[flancian]] note that the value/cost function is undefined; this is a procedure to find paths/tunnels
  - [[utopoi]]
    - [[jonathan]] a project that wants to build channels with related projects, one of this is this repository; but it is not exactly the same.
    - [[repository]] sounds static, whereas the [[m prime]] approach sounds more like a [[machine]].
    - [[jonathan]] [[noded]] [[utopia studies]]
    - [[flancian]] I liked [[topiary]]
- [[focus]]
- [[robin krom]]
  - [[jonathan]] thinks [[robin krom]] could be interested; related to [[category theory]], [[smart contracts]], [[financial markets]], [[liquid democracy]].
- [[reading group]]
- [[bug report]] [[agora plan]] https://anagora.org/node/reading-goup---christopher-alexander renders badly

## Telegram
### [[2020-12-18]]
- Jonathan thinks [[utopoi]] is not persistent; it will live for a while and then die, like an organism. But this applies mostly to the physical part; the digital part might endure.
- Knowing it has an end date actually feels nice.
- [[wittgenstein]]
  - likes that he critized his own work -- I agree.
  - part of his later work is about [[deconstructing]] his earlier work.
### [[2020-12-12]]
- "philosophy is the art of making questions enjoyable"
- pros/cons
- "feeling like a mashed potato"
- work is getting stressful due to an online course coming up in January.

### [[2020-12-11]]
- [[book]] [[asado verbal]]
  - "Mit Texten von [[Lucía Puenzo]], [[Pedro Mairal]], [[Washington Cucurto]], [[Fabián Casas]], [[Mariana Enríquez]], [[Cecilia Pavón]], [[Juan Diego Incardona]], [[Ariel Magnus]],[[ Julia Coria]], [[Felix Bruzzone]],[[ Romina Paula]], [[Edgardo Gonzalez Amer]], [[Oliverio Coelho]], [[Carlos Blasco]] und [[Andi Nachon]]."


[//begin]: # "Autogenerated link references for markdown compatibility"
[person]: person "Person"
[category theory]: category-theory "Category Theory"
[go]: go "Go"
[twitter]: twitter "Twitter"
[Smart Agora]: smart-agora "Smart Agora"
[project]: project "Project"
[utopoi]: utopoi "Utopoi"
[meet]: meet "Meet"
[todo]: todo "Todo"
[2021-02-06]: journal/2021-02-06 "2021-02-06"
[protopoi space]: protopoi-space "Protopoi Space"
[christopher alexander reading group]: christopher-alexander-reading-group "Christopher Alexander Reading Group"
[people]: people "People"
[metasj]: metasj "Metasj"
[bmann]: bmann "Bmann"
[karlicoss]: karlicoss "Karlicoss"
[protopoi]: protopoi "Protopoi"
[stoa]: stoa "Stoa"
[protopoi org]: protopoi-org "Protopoi Org"
[2021-01-23]: journal/2021-01-23 "2021-01-23"
[chestertons fence]: chestertons-fence "Chestertons Fence"
[jonathan]: jonathan "Jonathan"
[flancian]: flancian "Flancian"
[place]: place "Place"
[index]: index "index"
[world]: world "World"
[borges]: borges "Borges"
[anirudh badri]: anirudh-badri "Anirudh Badri"
[book club]: book-club "Book Club"
[armengolaltayo]: armengolaltayo "Armengolaltayo"
[push]: push "Push"
[quote]: quote "Quote"
[a pattern language]: a-pattern-language "A Pattern Language"
[feedback]: feedback "Feedback"
[agora plan]: agora-plan "Agora Plan"
[node]: node "Node"
[2021-01-01]: journal/2021-01-01 "2021-01-01"
[category theory for engineers]: category-theory-for-engineers "Category Theory for Engineers"
[category theory for programmers]: category-theory-for-programmers "Category Theory for Programmers"
[agora]: agora "Agora"
[users]: users "Users"
[stoas]: stoas "Stoas"
[hofstadter]: hofstadter "Hofstadter"
[utopia]: utopia "Utopia"
[missing devices]: missing-devices "Missing Devices"
[m prime]: m-prime "M Prime"
[noded]: noded "Noded"
[focus]: focus "Focus"
[liquid democracy]: liquid-democracy "Liquid Democracy"
[2020-12-18]: journal/2020-12-18 "2020-12-18"
[wittgenstein]: wittgenstein "Wittgenstein"
[2020-12-12]: journal/2020-12-12 "2020-12-12"
[2020-12-11]: journal/2020-12-11 "2020-12-11"
[book]: book "Book"
[asado verbal]: asado-verbal "Asado Verbal"
[Andi Nachon]: andi-nachon "Andi Nachon"
[//end]: # "Autogenerated link references"